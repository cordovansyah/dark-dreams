# Dark Dreams

Dark Dreams is a Unity game development project that I also happen to learn alongside my AR projects. It unfolds a zombie shooter like
skirmish in our nightmares / bad dreams. The flow is simple, we have to kill as much zombies as possible to gain bigger points, or eventually
lose when zombies take over our body and the game exits.

# Motivation
My first programming language was C++ and the book I read as it's reference was Robert Dawson's Introduction to Game Programming in C++.
Thus, I was enthralled in how game development process works, specifically in level design where I am able to modify / customized several 
challenges for players to encounter. Fast forward to 2018, I developed continuum curiosity to also learn how to build games in code, and
so I took an online course about Unity Game Development and here is one of the capstone projects that I completed.


## Built With

* Unity 2018

